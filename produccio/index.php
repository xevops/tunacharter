<?php

$enviat = false;

if (isset($_POST['email'])) {
    $mail = $_POST['email'];
    $tema = $_POST['asunto'];
    $missatge = $_POST['mensaje'];

    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $tema_mail = "contacte web";
    $correu = "info@tunachartercostabrava.com";

    $text = "<strong>Mail de:</strong> ".$mail."<br />"."<strong>Tema:</strong> ".$tema."<br />"."<strong>Misstage:</strong> "."<br />".$missatge;

    $enviat = mail($correu, $tema_mail, $text, $cabeceras);
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tuna Charter Costa brava. Salidas de pesca</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="xps">
    
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Fancybox
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/jquery.fancybox.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        @media screen and (max-width: 500px) {
            img.nosotros {
                height: 300px;
                width: 300px;
                border-radius: 50%;
                border: 5px solid #fcac45;
            }
            #tf-menu a.navbar-brand {
                font-size: 12px;
            }
            iframe {
                width: 300px;
            }
            .zonas {
                padding-top: 30px;
            }
            h2 {
                font-size: 25px;
                line-height: 25px;
            }
            a.fa.fa-angle-down {
                padding-top: 15px;
            }
            div.portada {
                display: none;
            }

            h1 {
                font-size: 30px;
            }
            p.lead {
                font-size: 20px;
            }
            img.parapesca {
                width: 300px;
            }
            p.margin-portada {
                margin-bottom: 70px;
            }
        }
    </style>
  </head>
  <body>
    <!-- Navigation
    ==========================================-->
    <nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand tipo" href="index.php">Tuna Charter Costa Brava</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#tf-home" class="page-scroll">Inicio</a></li>
            <li><a href="#tf-about" class="page-scroll">Nosotros</a></li>
            <li><a href="#tf-team" class="page-scroll">Equipo</a></li>
            <li><a href="#tf-services" class="page-scroll">Modalidades</a></li>
            <li><a href="#tf-works" class="page-scroll">El atún</a></li>
            <li><a href="#tf-testimonials" class="page-scroll">Zonas</a></li>
            <li><a href="#tf-media" class="page-scroll">Fotos y videos</a></li>
            <li><a href="#tf-contact" class="page-scroll">Contacto</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <!-- Home Page
    ==========================================-->
    <div id="tf-home" class="text-center">
        <div class="overlay">
            <div class="content">
                <div class="container">
                    <div class="row">
                         <div class="item col-md-3 portada">
                            <div class="thumbnail">
                                <img class="equipo" src="img/portada1.jpg" alt="..." class="img-circle team-img">
                            </div>
                        </div>
                        <div class="item col-md-3 portada">
                            <div class="thumbnail">
                                <img class="equipo" src="img/portada2.jpg" alt="..." class="img-circle team-img">
                            </div>
                        </div>
                        <div class="item col-md-3 portada">
                            <div class="thumbnail">
                                <img class="equipo" src="img/portada3.jpg" alt="..." class="img-circle team-img">
                            </div>
                        </div>
                        <div class="item col-md-3 portada">
                            <div class="thumbnail">
                                <img class="equipo" src="img/portada4.jpg" alt="..." class="img-circle team-img">
                            </div>
                        </div>
                    </div>
                </div>
               

                <h1>Bienvenido a <strong><span class="color tipo">Tuna Charter Costa Brava</span></strong></h1>
                <!--<p class="lead">Especialistas en la <strong>pesca del atún</strong> con más de <strong>15 años de experiencia</strong> y amplio conocimiento de las <strong>zonas de pesca</strong></p>
                <p class="lead">Ven a disfrutar de la pesca del atún en la <strong>Costa Brava</strong>, uno de los mejores puntos del mundo para el <strong>gigante del Mediterráneo</strong></p>-->
                <p class="lead margin-portada">Date el placer de disfrutar de la lucha con <strong>atunes</strong> de hasta <strong>más de 100kg en plena Costa Brava</strong></p>
                <h2><strong><span class="color">Solo practicamos captura y suelta</span></strong></h2>
                <p class="lead"><strong>Sponsored by</strong></p>
                <a href="http://www.parapesca.com/" target="_blank"><img class="parapesca" src="img/parapesca.png" alt="parapesca"></a>
                <div class="clearfix"></div>
                <!--<a href="#tf-about" class="fa fa-angle-down page-scroll"></a>-->
            </div>
        </div>
    </div>

    <!-- About Us Page
    ==========================================-->
    <div id="tf-about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img class="nosotros" src="img/climent.jpg" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <div class="about-text">
                        <div class="section-title">
                            <h4>Nosotros</h4>
                            <h2>Experiencia y <strong>profesionalidad</strong></h2>
                            <hr>
                            <div class="clearfix"></div>
                        </div>
                        <p class="intro">Climent Domingo, patrón de la Perla Negra, cuenta con más de 15 años de experiencia pescando el atún en todas sus modalidades.</p>
                        <p class="intro">Pionero en técnicas, antiguo colaborador de las revistas Pesca a Bordo y Jara y Sedal Pesca, así como protagonista de varios reportajes del canal Caza y Pesca, es un gran conocedor del comportamiento del atún y sus hábitos. </p>
                        <p class="intro">Ganador del último Campeonato de Catalunya de Pesca de Atún Gigante.</p>
                        <p class="intro">Nosotros te asesoramos de la mejor opción en cada momento para conseguir la captura de tus sueños.</p>
                        <p class="intro"><img class="language" src="img/english.png">We speak english</p>
                        <p class="intro"><img class="language" src="img/french.png">Nous parlons français</p>
                        <ul class="about-list">
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Experiencia</strong> - <em>más de 15 años de experiencia</em>
                            </li>
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Profesionalidad</strong> - <em>la mejor opción según la epoca del año</em>
                            </li>
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Capturas</strong> - <em>en pocas ocasiones nos vamos sin alguna captura</em>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Team Page
    ==========================================-->
    <div id="tf-team" class="text-center">
        <div class="overlay">
            <div class="container">
                <div class="section-title center">
                    <h2><strong>Equipo de pesca</strong></h2>
                    <div class="line">
                        <hr>
                    </div>
                </div>

                <!-- <div id="team" class="owl-carousel owl-theme row"> -->
                    <!-- <div class="item">
                        <div class="thumbnail">
                            <img src="img/team/01.jpg" alt="..." class="img-circle team-img">
                            <div class="caption">
                                <h3>Jenn Gwapa</h3>
                                <p>CEO / Founder</p>
                                <p>Do not seek to change what has come before. Seek to create that which has not.</p>
                            </div>
                        </div>
                    </div> -->

                    <div class="item col-md-6">
                        <div class="thumbnail">
                            <img class="equipo" src="img/barco.jpg" alt="..." class="img-circle team-img">
                            <div class="caption">
                                <h3>La Perla Negra</h3>
                                <p>Ágil, rápida y cómoda. Ideal para grupos de 3-4 pescadores, nuestra Hydra Sports 2400cc equipada con un Suzuki 300Cv es perfecta para perseguir pajareras y luchar con grandes ejemplares.</p>
                                <p>Equipada con la mejor electrónica Raymarine, vivero, 20 cañeros… ¡es una pura Fisher americana.</p>
                            </div>
                        </div>
                    </div>

                    <div class="item col-md-6">
                        <div class="thumbnail">
                            <img class="equipo" src="img/material.jpg" alt="..." class="img-circle team-img">
                            <div class="caption">
                                <h3>Cañas y carretes</h3>
                                <p>Disponemos de material de primera calidad para la lucha con grandes ejemplares. Carretes Shimano Tiagra, cañas Stand up mango curvo, infinidad de señuelos Halco, Hex Head, Williamson, Shimano…</p>
                            </div>
                        </div>
                    </div>

                    
                
            </div>
        </div>
    </div>

    <!-- Services Section
    ==========================================-->
    <div id="tf-services" class="text-center">
        <div class="container">
            <div class="section-title center">
                <h2><strong>Modalidades</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
                <p>Nosotros te asesoramos de la mejora técnica y zona según cada momento.</p>
                <p>El <strong>spinning</strong> es genial a nivel de emociones; los ataques con señuelos de superficie son simplemente espectaculares, ver los atunes perseguir nuestro señuelo, atacarlo con toda su furia y ¡!strike!!</p>
                <p>El <strong>brumeo</strong> es la pesca por excelencia del atún gigante; con nuestro rastro de sardina los hacemos emerger des del abismo de los cañones submarinos hasta tenerlo en nuestras cañas y conseguir clavar ejemplares de hasta más de 250kg.</p>
                <p>El <strong>curricán</strong> es la técnica más fácil y divertida para conseguir disfrutar de un buen numero de captura de atunes de mediano tamaño, buscar las pajareras y los atunes saltando, pasar nuestros señuelos y a por las picada múltiples!</p>
            </div>

            
        </div>
    </div>

    <!-- Clients Section
    ==========================================-->
    <div id="tf-clients" class="text-center">
        <div class="overlay">
            <div class="container">
                <div class="row">
                <div class="item col-md-4">
                    <div class="thumbnail">
                        <img class="modalidades" src="img/brumeo.jpg" alt="..." class="img-circle team-img">
                        <div class="caption">
                            <h4><strong>Brumeo</strong></h4>
                            <p><span class="color">En busca del atún gigante</span></p>
                            <p>Capturas entre 50 y 250 kg</p>
                            <p>Abril - Mayo | Julio - Agosto- Setiembre</p>
                            <p>Salida 6h: 600 €</p>
                            <p>Salida 9h: 750 €</p>
                        </div>
                    </div>
                </div>
                <div class="item col-md-4">
                     <div class="thumbnail">
                        <img class="modalidades" src="img/curri.jpg" alt="..." class="img-circle team-img">
                        <div class="caption">
                            <h4><strong>Currican</strong></h4>
                            <p><span class="color">A por las pajareras</span></p>
                            <p>Capturas entre 15 y 40kg</p>
                            <p>Julio - Agosto- Setiembre</p>
                            <p>Salida 6h: 600 €</p>
                            <p>Salida 9h: 750 €</p>
                        </div>
                    </div>
                </div>
                <div class="item col-md-4">
                     <div class="thumbnail">
                        <img class="modalidades" src="img/spinning.jpg" alt="..." class="img-circle team-img">
                        <div class="caption">
                            <h4><strong>Spinning</strong></h4>
                            <p><span class="color">Emociones fuertes</span></p>
                            <p>Capturas entre 15 y 40kg</p>
                            <p>Marzo - Abril - Mayo | Setiembre - Octubre</p>
                            <p>Salida 6h: 500 €</p>
                            <p>Salida 9h: 650 €</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Portfolio Section
    ==========================================-->
    <div id="tf-works">
        <div class="container"> <!-- Container -->
            <div class="section-title center">
                <h2 class="text-center"><strong>El atún</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img class="nosotros" src="img/atun.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-6">
                    <p>El atún rojo (Thunnus Thynnus) es un pez migratorio, que según la época del año habita en unas zonas concretas, además de cambiar sus hábitos y comportamientos según la temperatura del agua. Tenemos 2 épocas de pesca fuertes.</p>
                    <p><strong>Primavera</strong><br>A partir de marzo tenemos el atún muy cerca de la costa, alimentándose en superficie de los bancos de sardina y anchoa. Los encontramos entre la misma línea de costa y las 5 millas, con grandes pajareras donde el spinning nos ofrecerá los mejores resultados.</p>
                    <p>A medida que se va calentando el agua los grandes ejemplares entran en los cañones submarinos para alimentarse, por lo que a partir de mediados de abril hasta mediados de junio tenemos la 1ª temporada de brumeo con ejemplares de entre 50 y 100kg. Pescarlos en stand up es una auténtica gozada!</p>
                    <p><strong>Verano-otoño</strong><br>Durante los meses de junio y principios de julio el atún está poco activo, centrado en la reproducción. Seguidamente cuando el agua llega a los 20ºC los banco de atunes jóvenes se sitúan en las zonas de 1000-2000m de profundidad, donde tenemos corrientes frescas y ricas en las zonas exteriores de los cañones.. este es su momento de más actividad de todo el año pues llegan hambrientos de la reproducción.</p>
                    <p>A partir de finales de julio los verdaderos gigantes vuelven a entrar en los veriles submarinos cerca de la costa, descansando en las profundidades pero siempre listos para subir a la superficie a alimentarse de nuestra sardina. Agosto y septiembre son los mejores meses para los gigantes del brumeo.</p>
                    <p>A medida que avanza el verano el atún se va acercando a la costa, hasta que en otoño tenemos bancos de atún muy cerca para poderlos atacar de nuevo con spinning, aguantando cerca de la costa hasta la entrada del frío invernal.</p>
                </div>
                <div class="space"></div>    
            </div>
        </div>
    </div>

            <!--<div class="section-title text-center center">
                <h2>Take a look at <strong>our services</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
                <small><em>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</em></small>
            </div>
            <div class="space"></div>

            <div class="categories">
                
                <ul class="cat">
                    <li class="pull-left"><h4>Filter by Type:</h4></li>
                    <li class="pull-right">
                        <ol class="type">
                            <li><a href="#" data-filter="*" class="active">All</a></li>
                            <li><a href="#" data-filter=".web">Web Design</a></li>
                            <li><a href="#" data-filter=".photography">Photography</a></li>
                            <li><a href="#" data-filter=".app" >Mobile App</a></li>
                            <li><a href="#" data-filter=".branding" >Branding</a></li>
                        </ol>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div id="lightbox" class="row">

                <div class="col-sm-6 col-md-3 col-lg-3 branding">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/01.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3 photography app">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/02.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3 branding">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/03.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3 branding">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/04.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3 web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/05.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3 app">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/06.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3 photography web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/07.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3 web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Logo Design</h4>
                                    <small>Branding</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/portfolio/08.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> -->

    <!-- Testimonials Section
    ==========================================-->
    <div id="tf-testimonials">
        <div class="overlay">
            <div class="container">
                <div class="section-title center">
                    <h2 class="text-center"><strong>Zonas de pesca</strong></h2>
                    <div class="line">
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <img class="nosotros" src="img/barca.jpg" class="img-responsive">
                    </div>
                    <div class="col-md-6 zonas">
                        <p>Nuestro equipo tiene como  base Club Nàutic Port d’Aro, un puerto deportivo moderno situada en la localidad gerundense de Platja d’Aro, en el corazón de la Costa Brava.</p>
                        <p>Desde Port d’Aro tenemos diferentes zonas de pesca a nuestro alcance, con diferentes cañones submarinos muy productivos y abundantes de nuestro querido atún.</p>
                        <ul class="about-list">
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Zona sur: </strong><em>Cañón de Blanes y Miquela, los mejores Hot Spot para el brumeo</em>
                            </li>
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Zona este: </strong><em>La Fonera, la zona más activa de la Costa brava en verano</em>
                            </li>
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Zona norte: </strong><em>Cañon Begur, donde se mueven atunes de todas las medidas durante el año</em>
                            </li>
                        </ul>



                        <!--<div id="testimonial" class="owl-carousel owl-theme">
                            <div class="item">
                                <h5>This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</h5>
                                <p><strong>Dean Martin</strong>, CEO Acme Inc.</p>
                            </div>

                            <div class="item">
                                <h5>This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</h5>
                                <p><strong>Dean Martin</strong>, CEO Acme Inc.</p>
                            </div>

                            <div class="item">
                                <h5>This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</h5>
                                <p><strong>Dean Martin</strong>, CEO Acme Inc.</p>
                            </div>
                        </div>-->
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <!-- Media Section
    ==========================================-->
    <div id="tf-media">
        <div class="container">
            <div class="row">
                <div class="section-title center">
                <h2 class="text-center"><strong>FOTOS Y VIDEOS</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
                
                <div class="col-md-12 fila">
                    <a class="fancybox" rel="gallery1" href="img/media16.jpg" title="">
                        <img class="media" src="img/media16.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media2.jpg" title="">
                        <img class="media" src="img/media2.jpg" alt="" />
                    </a>

                    <div class="galeria-img">
                        <a class="fancybox vertical" rel="gallery1" href="img/media3.jpg" title="">
                            <img class="media" src="img/media3.jpg" alt="" />
                        </a>
                    </div>
                    
                    <a class="fancybox" rel="gallery1" href="img/media4.jpg" title="">
                        <img class="media" src="img/media4.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media5.jpg" title="">
                        <img class="media" src="img/media5.jpg" alt="" />
                    </a>
                    
                    <a class="fancybox" rel="gallery1" href="img/media6.jpg" title="">
                        <img class="media" src="img/media6.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media7.jpg" title="">
                        <img class="media" src="img/media7.jpg" alt="" />
                    </a>
                </div>
                
                <div class="col-md-12 fila">
                    <a class="fancybox" rel="gallery1" href="img/media8.jpg" title="">
                        <img class="media" src="img/media8.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media9.jpg" title="">
                        <img class="media" src="img/media9.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media10.jpg" title="">
                        <img class="media" src="img/media10.jpg" alt="" />
                    </a>

                    <div class="galeria-img">
                        <a class="fancybox vertical" rel="gallery1" href="img/media11.jpg" title="">
                            <img class="media" src="img/media11.jpg" alt="" />
                        </a>
                    </div>
                    
                    <a class="fancybox" rel="gallery1" href="img/media12.jpg" title="">
                        <img class="media" src="img/media12.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media13.jpg" title="">
                        <img class="media" src="img/media13.jpg" alt="" />
                    </a>
                    
                    <a class="fancybox" rel="gallery1" href="img/media14.jpg" title="">
                        <img class="media" src="img/media14.jpg" alt="" />
                    </a>
                </div>

                <div class="col-md-12 fila">
                    <a class="fancybox" rel="gallery1" href="img/media15.jpg" title="">
                        <img class="media" src="img/media15.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media17.jpg" title="">
                        <img class="media" src="img/media17.jpg" alt="" />
                    </a>
                    
                    <div class="galeria-img">
                        <a class="fancybox vertical" rel="gallery1" href="img/media18.jpg" title="">
                            <img class="media" src="img/media18.jpg" alt="" />
                        </a>
                    </div>

                    <a class="fancybox" rel="gallery1" href="img/media19.jpg" title="">
                        <img class="media" src="img/media19.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media21.jpg" title="">
                        <img class="media" src="img/media21.jpg" alt="" />
                    </a>

                    <div class="galeria-img">
                        <a class="fancybox" rel="gallery1" href="img/media24.jpg" title="">
                            <img class="media" src="img/media24.jpg" alt="" />
                        </a>
                    </div>
                    
                    <div class="galeria-img">
                        <a class="fancybox" rel="gallery1" href="img/media25.jpg" title="">
                            <img class="media" src="img/media25.jpg" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <a class="fancybox" rel="gallery1" href="img/media26.jpg" title="">
                        <img class="media" src="img/media26.jpg" alt="" />
                    </a>

                    <a class="fancybox" rel="gallery1" href="img/media27.jpg" title="">
                        <img class="media" src="img/media27.jpg" alt="" />
                    </a>
                </div>

                <div class="clearfix"></div>

                <div class="col-sm-12 col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/MCapFzpBmuY" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-12 col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/IRAknBzM680" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-12 col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/cgx5odMH_l0" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-12 col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/pGMiIndflIA" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-12 col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/lIF6P6OxOCU" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-12 col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/OptvV6K1jFE" frameborder="0" allowfullscreen></iframe>
                </div>

            </div>
            </div>
        </div>
    </div>



    <!-- Contact Section
    ==========================================-->
    <div id="tf-contact" class="text-center">
        <div class="container">
            <div class="row">
                <div class="section-title center">
                    <h2><strong>contacto</strong></h2>
                    <div class="line">
                        <hr>
                    </div>
                    <div class="clearfix"></div>          
                </div>
                <div class="col-md-12 mapa">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1062.1954687073578!2d3.0593810000000086!3d41.803295999999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDHCsDQ4JzExLjkiTiAzwrAwMyczMy44IkU!5e1!3m2!1sca!2ses!4v1426534873953" width="1140" height="400" frameborder="0" style="border:0"></iframe>                </div>
                <div class="col-md-6">
                    <address>
                        <p><strong>Tuna Charter Costa Brava</strong></p>
                        <p>Climent Domingo Esgleyes</p>
                        <p>Telefono: +34 657513428</p>
                        <p>Email: info@tunachartercostabrava.com</p>
                        <p>Carrer Punta Prima, 5</p>
                        <p>17250 Platja d'Aro</p>
                        <p>Girona (Spain)</p>
                    </address>
                </div>
                <div class="col-md-6">
                     <form action="#" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Asunto</label>
                                    <input type="text" class="form-control" id="asunto" name="asunto" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mensaje</label>
                            <textarea class="form-control" rows="3" name="mensaje"></textarea>
                        </div>
                        
                        <button type="submit" class="btn tf-btn btn-default">Enviar</button>
                    </form>
                    <div class="clearfix"></div> 
                    <?php 

                    if ($enviat) { ?>
                    <p>Su mensaje ha sido envido. Gràcias por contactar con nosotros.</p>
                    <?php } ?>
                </div>
                <!--<div class="col-md-8 col-md-offset-2">

                    <div class="section-title center">
                        <h2>Feel free to <strong>contact us</strong></h2>
                        <div class="line">
                            <hr>
                        </div>
                        <div class="clearfix"></div>
                        <small><em>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</em></small>            
                    </div>

                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Message</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        
                        <button type="submit" class="btn tf-btn btn-default">Submit</button>
                    </form>

                </div>-->
            </div>

        </div>
    </div>

    <nav id="footer">
        <div class="container">
            <div class="pull-left fnav">
                <!--<p>ALL RIGHTS RESERVED. COPYRIGHT © 2014. Designed by <a href="https://dribbble.com/shots/1817781--FREEBIE-Spirit8-Digital-agency-one-page-template">Robert Berki</a> and Coded by <a href="https://dribbble.com/jennpereira">Jenn Pereira</a></p>-->
            </div>
            
            <div class="col-md-6">
                <div class="pull-right fnav">
                <ul class="footer-social">
                    <li><a href="https://www.facebook.com/tunacostabrava" target="_blank"><i class="fa fa-facebook"></i></a></li>
                </ul>
            </div>
            </div>
            
        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/SmoothScroll.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <!-- Fancybox -->
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>

    <script src="js/owl.carousel.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect  : 'none',
                closeEffect : 'none'
            });
        });
    </script>
    

  </body>
</html>